import pygameMenu
from pygameMenu.locals import *

# FPS
FPS = 30

# color
BLACK	=	(0,		0,		0)
DARK_GRAY =	(50,	50,		50)
WHITE	=	(255,	255,	255)
RED		=	(255,	0,		0)
GREEN	=	(0,		255,	0)
BLUE	= 	(0,		0,		255)
YELLOW	= 	(255,	255,	0)


# display
SCREEN_WIDTH = 800
SCREEN_HEIGHT = 720

#Initial position
MAP_X = 0
MAP_Y = 0
FINISH_LINE_X = 350
FINISH_LINE_Y = 620
CAR_X = FINISH_LINE_X - 15
CAR_Y = FINISH_LINE_Y + 40
CAR_ANGLE = 0

#mapID
MENU_SCREEN_ID = 0
MAP_ID = 1
FINISH_LINE_ID = 2

#Text
ABOUT = ['Racing Game Version {}'.format('1.0'),
		'Author: {}'.format('Nguyen Thai Viet'),
		'Email: {}'.format('vietnguyen155@gmail.com'),
		PYGAMEMENU_TEXT_NEWLINE,]

FINISH = ['Run time: ',
		'Highest speed: ',
		'Gone distance: ',
		PYGAMEMENU_TEXT_NEWLINE,]
