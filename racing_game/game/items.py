import os
import math
import pygame
from .settings import *


class car(pygame.sprite.Sprite):

	###########################################################################
	# Function: __init__
	# Description: constructor for car
	# Input: x, y, angle
	# Output: None
	# Return: None
	###########################################################################
	def __init__(self, x, y, angle):
		pygame.sprite.Sprite.__init__(self)
		self.angle = CAR_ANGLE
		self.speed = 0
		self.org_image = pygame.image.load(os.path.join(os.path.dirname(__file__), '..', 'resources', 'images', 'car.png')).convert_alpha()
		#self.org_image.set_colorkey(WHITE)
		self.org_rect = self.org_image.get_rect(center = (x,y))

		self.image = pygame.transform.rotate(self.org_image, self.angle).convert_alpha()
		#self.image.set_colorkey(WHITE)
		self.rect = self.image.get_rect(center = self.org_rect.center)
		self.mask = pygame.mask.from_surface(self.image, 50)
		self.collision = False
		self.distance = [300 for i in range(5)]
		self.gone_distance = 0



	###########################################################################
	# Function: set_angle
	# Description: set car angle
	# Input: value
	# Output: None
	# Return: None
	###########################################################################
	def set_angle(self, value):
		self.angle = value
		self.image = pygame.transform.rotate(self.org_image, value).convert_alpha().convert_alpha()
		self.rect = self.image.get_rect(center = self.org_rect.center)
		self.mask = pygame.mask.from_surface(self.image, 50)



	###########################################################################
	# Function: set_position
	# Description: set car position
	# Input: x, y
	# Output: None
	# Return: None
	###########################################################################
	def set_position(self, x, y):
		self.rect.center = (x, y)
		self.org_rect.center = self.rect.center
		self.mask = pygame.mask.from_surface(self.image, 50)
		self.speed = 0



	###########################################################################
	# Function: check_distance
	# Description: check the distance from car to obstacle
	# Input: obstacle
	# Output: None
	# Return: None
	###########################################################################
	def check_distance(self, obstacle):
		dis_num = len(self.distance)
		for i in range(dis_num):
			rad = math.radians(self.angle + (i - int(dis_num/2))*45)
			for dis in range(0, 300):
				dx = int(dis * math.cos(rad))
				dy = int(dis * math.sin(rad))
				if obstacle.mask.get_at((self.rect.center[0] + dx, self.rect.center[1] - dy)) == 1:
					self.distance[i] = dis
					break
			else:
				self.distance[i] = 300
		return self.distance



	###########################################################################
	# Function: accel
	# Description: accelerate the car speed
	# Input: None
	# Output: None
	# Return: None
	###########################################################################
	def accel(self, value):
		self.speed += value
		if self.speed < 0:
			self.speed = 0
		elif self.collision == True and self.speed > 10:
			self.speed = 10
		elif self.collision == False and self.speed > 50:
			self.speed = 50



	###########################################################################
	# Function: steer
	# Description: steer the car
	# Input: steer_value
	# Output: None
	# Return: None
	###########################################################################
	def steer(self, steer_value):
		self.angle += steer_value
		if self.angle < -360:
			self.angle += 360
		elif self.angle >= 360:
			self.angle -= 360

		self.image = pygame.transform.rotate(self.org_image, self.angle).convert_alpha().convert_alpha()
		self.rect = self.image.get_rect(center = self.org_rect.center)
		self.mask = pygame.mask.from_surface(self.image, 50)




	###########################################################################
	# Function: draw_distance
	# Description: draw distance line
	# Input: scr
	# Output: None
	# Return: None
	###########################################################################
	def draw_distance(self, scr):
		dis_num = len(self.distance)
		for i in range(dis_num):
			rad = math.radians(self.angle + (i - int(dis_num/2))*45)
			dx = int(self.distance[i] * math.cos(rad))
			dy = int(self.distance[i] * math.sin(rad))
			pygame.draw.line(scr, RED, self.rect.center, (self.rect.center[0] + dx, self.rect.center[1] - dy), 1)
			pygame.draw.circle(scr, YELLOW, (self.rect.center[0] + dx, self.rect.center[1] - dy), 2, 2)



class map(pygame.sprite.Sprite):

	###########################################################################
	# Function: __init__
	# Description: constructor for class map
	# Input: x, y, map_id
	# Output: None
	# Return: None
	###########################################################################
	def __init__(self, x, y, map_id):
		pygame.sprite.Sprite.__init__(self)
		self.image = pygame.image.load(os.path.join(os.path.dirname(__file__), '..', 'resources', 'images', 'map_' + str(map_id) +'.png')).convert_alpha()
		self.rect = self.image.get_rect()
		self.rect.x = x
		self.rect.y = y
		self.mask = pygame.mask.from_surface(self.image, 50)

