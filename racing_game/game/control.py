import sys
import math
import pygame
import time
import logging
import pygameMenu
from pygameMenu.locals import *
from .items import *
from .settings import *




class Control:

	###########################################################################
	# Function: __init__
	# Description: constructor for Control
	# Input: None
	# Output: None
	# Return: None
	###########################################################################
	def __init__(self):
		#--------------Init Display--------------
		self.screen = pygame.display.set_mode((SCREEN_WIDTH, SCREEN_HEIGHT))
		self.bgcolor = DARK_GRAY
		self.fps = FPS
		self.clock = pygame.time.Clock()
		pygame.display.set_caption("Racing game")



		#--------------Init objects--------------
		self.map = map(MAP_X, MAP_Y, MAP_ID)
		self.finish_line = map(FINISH_LINE_X, FINISH_LINE_Y, FINISH_LINE_ID)
		self.my_car = car(CAR_X, CAR_Y, CAR_ANGLE)
		self.car_list = pygame.sprite.Group()
		self.car_list.add(self.my_car)
		self.map_list = pygame.sprite.Group().add(self.map)
		self.menu_background = map(MAP_X, MAP_Y, MENU_SCREEN_ID)



		#--------------Operation variable-------------
		self.mode = 0					#0: manual, 1: auto, 2: training
		self.distance_display = 0
		self.info_display = 0
		self.game_running = False
		self.finish = False
		self.finish_result = False
		self.collision = False
		self.on_finish_line = True
		self.exit = False



		#--------------Recorded information-------------
		self.start_time = 0
		self.finish_time = 0
		self.highest_speed = 0


		#--------------Init Menu--------------
		self.modelist = [('Manual', 0), ('Auto', 1), ('Training', 2)]
		self.onofflist = [('OFF',0), ('ON',1)]

		#--------------Setting Menu--------------
		self.settings_menu = pygameMenu.Menu(	self.screen,
												dopause=False,
												enabled=False,
												font=pygameMenu.fonts.FONT_NEVIS,
												menu_alpha=60,
												menu_color=BLACK,
												menu_height=300,
												menu_width=600,
												onclose=PYGAME_MENU_RESET,
												rect_width=4,
												title='Settings',
												title_offsety=5,
												window_height=SCREEN_HEIGHT,
												window_width=SCREEN_WIDTH
											)
		self.settings_menu.add_selector(	'Running Mode ',
											self.modelist,
											default=0,
											onchange=self.menu_change_mode,
											onreturn=self.menu_change_mode,
										)
		self.settings_menu.add_selector(	'Distance display ',
											self.onofflist,
											default=0,
											onchange=self.menu_distance_display,
											onreturn=self.menu_distance_display,
										)
		self.settings_menu.add_selector(	'Info display ',
											self.onofflist,
											default=0,
											onchange=self.menu_info_display,
											onreturn=self.menu_info_display,
										)
		self.settings_menu.add_option('Return to Main Menu', PYGAME_MENU_BACK)


		#--------------About Menu--------------
		self.about_menu = pygameMenu.TextMenu(	self.screen,
												dopause=False,
												draw_text_region_x=50,
												font=pygameMenu.fonts.FONT_NEVIS,
												font_size_title=30,
												font_title=pygameMenu.fonts.FONT_8BIT,
												menu_color_title=BLUE,
												onclose=PYGAME_MENU_DISABLE_CLOSE,
												text_centered=True,
												text_fontsize=20,
												title='About',
												window_height=SCREEN_HEIGHT,
												window_width=SCREEN_WIDTH
											)
		self.about_menu.add_option('Return to Menu', PYGAME_MENU_BACK)

		for m in ABOUT:
			self.about_menu.add_line(m)
		self.about_menu.add_line(PYGAMEMENU_TEXT_NEWLINE)


		#--------------Main Menu--------------
		self.main_menu = pygameMenu.Menu(	self.screen,
											dopause=False,
											font=pygameMenu.fonts.FONT_NEVIS,
											menu_alpha=60,
											menu_centered=True,
											menu_height=300,
											menu_width=600,
											onclose=PYGAME_MENU_CLOSE,
											title='Main Menu',
											title_offsety=5,
											window_width=SCREEN_WIDTH,
											window_height=SCREEN_HEIGHT
										)
		self.main_menu.add_option('Start Game', self.menu_start_game)
		self.main_menu.add_option(self.settings_menu.get_title(), self.settings_menu)
		self.main_menu.add_option(self.about_menu.get_title(), self.about_menu)
		self.main_menu.add_option('Exit', PYGAME_MENU_EXIT)


		#--------------Finish Menu--------------
		self.finish_menu = pygameMenu.TextMenu(	self.screen,
												dopause=False,
												enabled=False,
												draw_text_region_x=20,
												font=pygameMenu.fonts.FONT_NEVIS,
												font_size_title=30,
												font_title=pygameMenu.fonts.FONT_8BIT,
												menu_color_title=BLUE,
												onclose=PYGAME_MENU_DISABLE_CLOSE,
												text_centered=False,
												text_fontsize=30,
												title='GAME OVER',
												window_height=SCREEN_HEIGHT,
												window_width=SCREEN_WIDTH
												)
		self.finish_menu.add_option('Main Menu', PYGAME_MENU_CLOSE)
		self.finish_menu.add_option('Exit', PYGAME_MENU_EXIT)

		for m in FINISH:
			self.finish_menu.add_line(m)



	###########################################################################
	# Function: menu_change_mode
	# Description: function call when change game mode in menu
	# Input: None
	# Output: None
	# Return: None
	###########################################################################
	def menu_change_mode(self, game_mode):
		logging.info("menu_change_mode: change mode to %d" % game_mode)

		self.mode = game_mode



	###########################################################################
	# Function: menu_info_display
	# Description: display game info
	# Input: None
	# Output: None
	# Return: None
	###########################################################################
	def menu_info_display(self, avail):
		logging.info("menu_info_display: info display on/off: %d" % avail)

		self.info_display = avail



	###########################################################################
	# Function: menu_distance_display
	# Description: display distance
	# Input: None
	# Output: None
	# Return: None
	###########################################################################
	def menu_distance_display(self, avail):
		logging.info("menu_distance_display: distance display on/off: %d" % avail)

		self.distance_display = avail



	###########################################################################
	# Function: menu_main
	# Description: go to main menu
	# Input: None
	# Output: None
	# Return: None
	###########################################################################
	def menu_main(self):
		logging.info("menu_start_game: game mode = %d" % self.mode)

		if self.finish_menu.is_enabled():
			self.finish_menu.disable()

		self.main_menu.enable()

		if self.game_running == True:
			self.game_running = False



	###########################################################################
	# Function: menu_start_game
	# Description: start game
	# Input: None
	# Output: None
	# Return: None
	###########################################################################
	def menu_start_game(self):
		logging.info("menu_start_game: game mode = %d" % self.mode)

		self.main_menu.disable()
		self._reset()
		self.game_running = True
		self.start_time = time.time()



	###########################################################################
	# Function: check_event
	# Description: check events from keyboard
	# Input: None
	# Output: None
	# Return: None
	###########################################################################
	def check_event(self):
		logging.debug("check_event")

		if self.game_running == True:
			#--------------Event for game--------------
			if self.mode == 0:					#Manual Mode
				key_pressed = pygame.key.get_pressed()
				if key_pressed[pygame.K_LEFT]:
					self.my_car.steer(5)
				if key_pressed[pygame.K_RIGHT]:
					self.my_car.steer(-5)

			for event in pygame.event.get():
				if event.type == pygame.QUIT:
					logging.info("check_event:----------QUIT----------")
					self.game_running = False
					self.exit = True

				if event.type == pygame.KEYDOWN:
					if self.mode == 0:
						if event.key == pygame.K_UP:
							self.my_car.accel(1)
						elif event.key == pygame.K_DOWN:
							self.my_car.accel(-1)

					if event.key == pygame.K_ESCAPE:
						self.main_menu.enable()
						self.game_running = False

		else:
			#--------------Event for menu--------------
			events = pygame.event.get()
			if self.finish_menu.is_enabled():
				self.finish_menu.mainloop(events)
			else:
				self.main_menu.mainloop(events)

			for event in events:
				if event.type == pygame.QUIT:
					logging.info("check_event:----------QUIT----------")
					self.game_running = False
					self.exit = True
				elif event.type == pygame.KEYDOWN:
					if event.key == pygame.K_ESCAPE:
						self.main_menu.disable()
						self.game_running = False
						self.exit = True



	###########################################################################
	# Function: update_move
	# Description: update new position of a car
	# Input: car
	# Output: None
	# Return: None
	###########################################################################
	def update_move(self, car):
		logging.debug("update_move()")

		if car.speed > 0:
			rad = math.radians(car.angle)
			dx = int(car.speed * 50/FPS * math.cos(rad))
			dy = int(car.speed * 50/FPS * math.sin(rad))

			tmpx = car.org_rect.center[0] + dx
			tmpy = car.org_rect.center[1] - dy
			if tmpx > SCREEN_WIDTH:
				tmpx -= SCREEN_WIDTH
			elif tmpx < 0:
				tmpx += SCREEN_WIDTH
			if tmpy > SCREEN_HEIGHT:
				tmpy -= SCREEN_HEIGHT
			elif tmpy < 0:
				tmpy += SCREEN_HEIGHT

			car.rect.center = (tmpx, tmpy)
			car.org_rect.center = car.rect.center
			car.mask = pygame.mask.from_surface(car.image, 50)

			if self.my_car.speed > self.highest_speed:
				self.highest_speed = self.my_car.speed

			self.my_car.gone_distance += (dx**2 + dy**2) ** (0.5)


	###########################################################################
	# Function: check_collision
	# Description: Check collision between car and wall
	# Input: None
	# Output: None
	# Return: None
	###########################################################################
	def check_collision(self):
		logging.debug("check_collision()")

		if pygame.sprite.collide_mask(self.my_car, self.map) is not None:
			logging.info("check_collision:----------COLLISION----------")
			self.collision = True
			self.finish = True
			self.finish_time = time.time()
			self.finish_result = False
			return True
		else:
			self.collision = False
			return False



	###########################################################################
	# Function: check_finish
	# Description: Check whether car get the finish line
	# Input: None
	# Output: None
	# Return: None
	###########################################################################
	def check_finish(self):
		logging.debug("check_finish()")

		if pygame.sprite.collide_mask(self.my_car, self.finish_line) is not None:
			if self.on_finish_line == False and self.my_car.rect.x < self.finish_line.rect.x:
				logging.info("check_finish:----------YOU WIN----------")
				self.my_car.speed = 0
				self.on_finish_line = True
				self.finish = True
				self.finish_time = time.time()
				self.finish_result = True
		else:
			if self.on_finish_line == True and self.my_car.rect.x < self.finish_line.rect.x:
				self.finish = True
				self.finish_time = time.time()
				self.finish_result = False
			else:
				self.on_finish_line = False



	###########################################################################
	# Function: display_reset
	# Description: Reset all screen display
	# Input: None
	# Output: None
	# Return: None
	###########################################################################
	def display_reset(self):
		logging.debug("display_reset()")

		if self.game_running == True:
			self.screen.fill(self.bgcolor)
		else:
			self.screen.blit(self.menu_background.image, self.menu_background.rect)



	###########################################################################
	# Function: display_update
	# Description: Update display
	# Input: None
	# Output: None
	# Return: None
	###########################################################################
	def display_update(self):
		logging.debug("display_update()")

		if self.game_running == True:
			#--------------Display for game--------------
			if self.finish == False:
				#self.screen.fill(self.bgcolor)
				self.screen.blit(self.map.image, self.map.rect)
				self.screen.blit(self.finish_line.image, self.finish_line.rect)
				self.car_list.draw(self.screen)
				if self.distance_display == 1:
					self.my_car.draw_distance(self.screen)

				if self.info_display == 1:
					#---Display game mode---
					myfont = pygame.font.SysFont("monospace", 12)
					mode_str = "Game Mode: "
					if self.mode == 0:
						mode_str += "Manual"
					elif self.mode == 1:
						mode_str += "Auto"
					elif self.mode == 2:
						mode_str += "Training"
					label = myfont.render(mode_str, 1, (255, 255, 0))
					self.screen.blit(label, (10, 0))

					# ---Display run time---
					time_str = "Run time: %d" % (time.time() - self.start_time)
					label = myfont.render(time_str, 1, (255, 255, 0))
					self.screen.blit(label, (10, 10))

					# ---Display speed---
					speed_str = "Speed: %d" % self.my_car.speed
					label = myfont.render(speed_str, 1, (255, 255, 0))
					self.screen.blit(label, (10, 20))

					# ---Display speed---
					gone_str = "Gone distance: %d" % self.my_car.gone_distance
					label = myfont.render(gone_str, 1, (255, 255, 0))
					self.screen.blit(label, (10, 30))

					# ---Display training info---
					if self.mode == 2:
						epoch_str = "Epoch: %d" % self.my_car.speed
						label = myfont.render(epoch_str, 1, (255, 255, 0))
						self.screen.blit(label, (10, 40))

			else:
				self.game_running = False
				# --------------Display finish menu--------------
				run_time = self.finish_time - self.start_time

				if self.finish_result == True:
					self.finish_menu.set_title('YOU WIN')
				else:
					self.finish_menu.set_title('GAME OVER')

				for m in range(len(self.finish_menu._text)):
					if self.finish_menu._text[m].find('Run time:') != -1:
						self.finish_menu._text[m] = 'Run time: ' + '{0:.2f}'.format(run_time) + ' second(s)'
					elif self.finish_menu._text[m].find('Highest speed:') != -1:
						self.finish_menu._text[m] = 'Highest speed: ' + '{:d}'.format(self.highest_speed)
					elif self.finish_menu._text[m].find('Gone distance:') != -1:
						self.finish_menu._text[m] = 'Gone distance: ' + '{0:.2f}'.format(self.my_car.gone_distance)

				self.finish_menu.enable()
		else:
			#--------------Display for menu--------------
			if self.finish_menu.is_disabled() and self.main_menu.is_disabled():
				self.main_menu.enable()

		pygame.display.update()



	###########################################################################
	# Function: run
	# Description: main loop
	# Input: None
	# Output: None
	###########################################################################
	def run(self):
		logging.debug('run()')

		self.main_menu.enable()

		while self.exit == False:
			self.display_reset()
			self.check_event()

			if self.game_running == True and self.mode == 0:
				self.update_move(self.my_car)
				self.my_car.check_distance(self.map)

				if True == self.check_collision():
					pass
				else:
					self.check_finish()

			self.display_update()
			self.clock.tick(self.fps)



	###########################################################################
	# Function: _step
	# Description: execute the next action
	# Input: None
	# Output: None
	###########################################################################
	def _step(self, action):
		logging.info("_step")

		if action == 0: 			#UP
			self.my_car.accel(1)
		elif action == 1:			#DOWN
			self.my_car.accel(-1)
		elif action == 2:			#LEFT
			self.my_car.steer(5)
		elif action == 3:			#RIGHT
			self.my_car.steer(-5)

		self.update_move(self.my_car)
		distance_list = self.my_car.check_distance(self.map)

		if True == self.check_collision():
			pass
		else:
			self.check_finish()

		return [distance_list, self.my_car.speed, self.my_car.gone_distance, self.finish, self.finish_result]


	###########################################################################
	# Function: _reset
	# Description: reset game
	# Input: None
	# Output: None
	# Return: None
	###########################################################################
	def _reset(self):
		logging.info("_reset")

		self.my_car.set_position(CAR_X, CAR_Y)
		self.my_car.set_angle(0)
		self.my_car.speed = 0
		self.my_car.gone_distance = 0
		self.start_time = time.time()
		self.finish_time = 0
		self.finish = False
		self.collision = False
		self.on_finish_line = True
