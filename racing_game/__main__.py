import game
import sys
import pygame
import threading
import logging



logging.basicConfig(format='[%(thread)d] - [%(levelname)s] - %(message)s', level=logging.INFO)

if __name__ == '__main__':
	logging.info("Main start")
	pygame.init()

	game_control = game.Control()
	game_thread = threading.Thread(target=game_control.run(), args=(1,))
	game_thread.start()

	#AI is not supported
	#brain_control = brain.Control()
	#brain = threading.Thread(target=brain_control.run(), args=(1,))
	#brain.start()

	pygame.quit()
	sys.exit()
